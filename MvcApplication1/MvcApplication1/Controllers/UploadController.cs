﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    public class UploadController : ApiController
    {
        public HttpResponseMessage Post()
        {
            HttpFileCollection files = HttpContext.Current.Request.Files;
            NameValueCollection form = HttpContext.Current.Request.Form;

            HttpPostedFile file = files[0];
            file.SaveAs(@"C:\Users\ben-hur\Desktop\DoJo Asp.Net Web Api\upload\teste.jpeg");
            
            return Request.CreateResponse(HttpStatusCode.OK, new { Nome = form["nome"] });
        }
    }
}
