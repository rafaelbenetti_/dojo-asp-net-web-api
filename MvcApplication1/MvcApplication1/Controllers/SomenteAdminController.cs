﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    [Authorize(Roles="Admin")]
    [RoutePrefix("api/somente-admin-controller")]
    public class SomenteAdminController : ApiController
    {
        [HttpGet]
        [Route("ok")]
        public HttpResponseMessage Ok()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { SomenteAdmin = true });
        }
    }
}
