﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MvcApplication1.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("cors")]
    public class CorsController : ApiController
    {
        [DisableCors]
        [HttpGet]
        [Route("sem-permissao-de-cors")]
        public HttpResponseMessage SemPermissaoDeCors()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { CORS = true });
        }

        [HttpPost]
        [Route("post-sem-permissao-de-cors")]
        public HttpResponseMessage PostSemPermissaoDeCors()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { CORS = true });
        }
    }
}
