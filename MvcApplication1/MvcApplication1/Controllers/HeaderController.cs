﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    [RoutePrefix("header")]
    public class HeaderController : ApiController
    {
        [HttpPost]
        [Route("enviar-header")]
        public HttpResponseMessage EnviarHeader()
        {
            IEnumerable<string> meusHeaders = Request.Headers.GetValues("Meu-Header");

            string header = meusHeaders.First();

            return Request.CreateResponse(HttpStatusCode.OK, new { Header = header });
        }
    }
}
