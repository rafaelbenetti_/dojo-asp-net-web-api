﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    [RoutePrefix("api/cookie")]
    public class CookieController : ApiController
    {
        const string COOKIE_NAME = "teste";

        [HttpPost]
        [Route("criar-cookie")]
        public HttpResponseMessage CriarCookie()
        {
            var cookie = new CookieHeaderValue(COOKIE_NAME, "12345");
            cookie.Expires = DateTimeOffset.Now.AddDays(1);

            // o chrome não aceita localhost ¬¬,
            // mas use em seu site quando publicado =)
            //cookie.Domain = Request.RequestUri.Host;

            cookie.Path = "/";

            HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.OK,
                new
                {
                    CookieName = COOKIE_NAME,
                    CookieVal = "12345"
                });

            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });

            return resp;
        }


        [HttpGet]
        [Route("buscar-cookie")]
        public HttpResponseMessage BuscarCookie()
        {
            CookieHeaderValue cookie = Request.Headers.GetCookies(COOKIE_NAME).FirstOrDefault();

            if (cookie != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        CookieName = COOKIE_NAME,
                        CookieVal = cookie[COOKIE_NAME].Value
                    });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Gone);
            }
        }
    }
}
