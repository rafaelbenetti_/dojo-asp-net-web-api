﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    [RoutePrefix("loja")]
    public class LojaController : ApiController
    {
        [HttpGet]
        [Route("buscar-pedidos")]
        public IEnumerable<string> BuscarPedidos()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        [Route("buscar-pedido/{idPedido}")]
        public HttpResponseMessage BuscarPedido(int idPedido)
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { IdPedido = idPedido });
        }

        // Só aceitará se o idPedido for um inteiro e o codigoItem for string somente com letras
        [HttpGet]
        [Route("buscar-item-do-pedido/{idPedido:int}/{codigoItem:alpha}")]
        public HttpResponseMessage BuscarPedido(int idPedido, string codigoItem)
        {
            return Request.CreateResponse(HttpStatusCode.OK, 
                new 
                { 
                    IdPedido = idPedido, 
                    CodigoItem = codigoItem 
                });
        }

        [HttpGet]
        [Route("buscar-status-do-pedido/{idPedido:int:min(1):max(10)}")]
        public HttpResponseMessage BuscarStatusPedido(int idPedido)
        {
            return Request.CreateResponse(HttpStatusCode.OK,
                new
                {
                    IdPedido = idPedido
                });
        }
    }
}
