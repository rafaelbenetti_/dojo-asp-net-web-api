﻿using MvcApplication1.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    //[NomeDaControlerFilter("HandlerFiltersController")]
    [AllowAnonymous]
    [RoutePrefix("handler-filters")]
    public class HandlerFiltersController : ApiController
    {
        [HttpGet]
        [IncluirHeaderFilter]
        [Route("incluir-header")]
        public HttpResponseMessage IncluirHeader()
        {
            string testHeader = Request.Headers.GetValues("Teste-Header").FirstOrDefault();

            return Request.CreateResponse(HttpStatusCode.OK, new { TesteHeaderValue = testHeader });
        }

        [HttpGet]
        [NaoImplementadoFilter]
        [Route("nao-implementado")]
        public HttpResponseMessage NaoImplementado()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("alterar-requisicao-message-handler")]
        public HttpResponseMessage AlterarRequisicaoMessageHandler()
        {
            string novoHeader = Request.Headers.GetValues("Alterar-Requisicao-Message-Handler").First();

            return Request.CreateResponse(HttpStatusCode.OK, new { NovoHeader = novoHeader });
        }

        [HttpPost]
        [Route("cancelar-requisicao-post-message-handler")]
        public HttpResponseMessage CancelarRequisicaoPostMessageHandler()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [HttpGet]
        [LogarExceptionFilter]
        [Route("get-com-exception")]
        public HttpResponseMessage GetComException()
        {
            throw new Exception("Ó a exception!!!");
        }
    }
}
