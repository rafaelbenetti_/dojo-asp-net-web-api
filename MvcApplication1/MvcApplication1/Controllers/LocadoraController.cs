﻿using MvcApplication1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    public class LocadoraController : ApiController
    {
        public void Post([FromBody]Filme filme)
        {

        }

        [HttpGet]
        [Route("~/api/v2/filmes/buscar-filmes")]
        public HttpResponseMessage BuscarFilmes()
        {
            var filmes = new List<Filme>();
            var categoriaTerror = new Categoria()
            {
                Id = 1,
                Nome = "Terror"
            };
            var categoriaComedia = new Categoria()
            {
                Id = 2,
                Nome = "Comédia"
            };

            filmes.Add(new Filme()
                {
                    Categoria = categoriaTerror,
                    Id = 1,
                    Preco = 1.99,
                    Titulo = "Marte Ataca!"
                });

            filmes.Add(new Filme()
            {
                Categoria = categoriaComedia,
                Id = 1,
                Preco = 200.99,
                Titulo = "Marte Ataca!"
            });

            return Request.CreateResponse(HttpStatusCode.OK, new { Filmes = filmes });
        }
    }
}
