﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("todos-podem-acessar")]
        public HttpResponseMessage CasaDaMaeJoana()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { TodosPodemAcessar = true });
        }

        [HttpPost]
        [Authorize]
        [Route("validar-autenticacao")]
        public HttpResponseMessage ValidarAutenticacao()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { Autenticado = true });
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("somente-admin")]
        public HttpResponseMessage SomenteAdmin()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { EhAdmin = true });
        }

        [HttpGet]
        [Authorize(Users = "ben-hur")]
        [Route("area-benhur")]
        public HttpResponseMessage AreaBenHur()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { AreaBenHur = true });
        }

        [HttpGet]
        [Authorize]
        [Route("validar-role-em-tempo-execucao")]
        public HttpResponseMessage ValidarRoleEmTempoExecucao()
        {
            if (User.IsInRole("Admin"))
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { ValidadoRuntime = true });
            }
                
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [HttpGet]
        [Authorize]
        [Route("validar-usuario-em-tempo-execucao")]
        public HttpResponseMessage ValidarUserNameEmTempoExecucao()
        {
            if (User.Identity.Name.Equals("ben-hur"))
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { ValidadoRuntime = true });
            }

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }

}
