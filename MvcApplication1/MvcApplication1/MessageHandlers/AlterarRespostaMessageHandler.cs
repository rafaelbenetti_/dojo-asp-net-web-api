﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MvcApplication1.MessageHandlers
{
    public class AlterarRespostaMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return base.SendAsync(request, cancellationToken).ContinueWith(
            (task) =>
                {
                    HttpResponseMessage response = task.Result;
                    response.Headers.Add("X-Alterar-Resposta-MessageHandler", "Teste");
                    return response;
                }
            );
        }
    }
}