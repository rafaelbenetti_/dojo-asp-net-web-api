﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MvcApplication1.MessageHandlers
{
    public class AlterarRequisicaoMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Add("Alterar-Requisicao-Message-Handler", "teste");

            return base.SendAsync(request, cancellationToken);
        }
    }
}