﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MvcApplication1.MessageHandlers
{
    public class CancelarRequisicaoPostMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            IEnumerable<string> chaveUsuarioHeaders = null;
            request.Headers.TryGetValues("X-Chave-Usuario", out chaveUsuarioHeaders);

            string chaveUsuario = chaveUsuarioHeaders == null ? null : chaveUsuarioHeaders.FirstOrDefault();

            if (String.IsNullOrEmpty(chaveUsuario) || chaveUsuario.Equals("123456"))
            {
                if (request.Method == HttpMethod.Post)
                {
                    HttpResponseMessage response = request.CreateResponse(HttpStatusCode.Unauthorized, new { Mensagem = "Chave Desabilitada" });

                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}