﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace MvcApplication1.Filters
{
    public class NomeDaControlerFilterAttribute : ActionFilterAttribute
    {
        public string NomeController { get; set; }

        public NomeDaControlerFilterAttribute(string nomeController)
        {
            NomeController = nomeController;
        }


        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            actionExecutedContext.Response.Headers.Add("Nome-Controller", NomeController);

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}