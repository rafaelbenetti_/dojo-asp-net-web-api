﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;

namespace MvcApplication1.Filters
{
    public class LogarExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            actionExecutedContext.Response = 
                actionExecutedContext.Request.CreateResponse(
                    HttpStatusCode.BadRequest, 
                    new { Exception = actionExecutedContext.Exception.Message });

            base.OnException(actionExecutedContext);
        }
    }
}