﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace MvcApplication1.Filters
{
    public class IncluirHeaderFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            actionContext.Request.Headers.Add("Teste-Header", "Testando...");

            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Request.Headers.GetValues("Teste-Header").Any())
            {
                actionExecutedContext.Response.Headers.Add("Custom-Response-Header", "Hello Jude!");
            }

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}