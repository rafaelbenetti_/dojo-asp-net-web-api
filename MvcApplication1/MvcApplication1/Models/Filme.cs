﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Filme
    {
        public int Id { get; set; }

        public string Titulo { get; set; }

        public double Preco { get; set; }

        public Categoria Categoria { get; set; }
    }
}