﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace MvcApplication1.Attributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            // LOGICA PARA PULAR A AUTENTICAÇÃO
            if (!SkipAuthorization(actionContext))
            {
                string userName = null;
                // SEU VALIDADOR de LOGIN
                bool isValid = IsValid(actionContext.Request, out userName);

                if (isValid)
                {
                    var identity = new GenericIdentity(userName);
                    IPrincipal principal = new GenericPrincipal(identity,
                                                                // SUAS PERMISSÕES (string[])
                                                                CarregarPermissoes(userName));

                    Thread.CurrentPrincipal = principal;
                    if (HttpContext.Current != null)
                    {
                        HttpContext.Current.User = principal;
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }

            base.OnAuthorization(actionContext);
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            Contract.Assert(actionContext != null);

            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }

        private static bool IsValid(HttpRequestMessage request, out string username)
        {
            username = null;
            var header = request.Headers.Authorization;

            if (header != null && header.Scheme == "Basic")
            {
                var credentials = header.Parameter;

                if (!string.IsNullOrWhiteSpace(credentials))
                {
                    var decodedCredentials =
                        Encoding.Default.GetString(Convert.FromBase64String(credentials));

                    var separator = decodedCredentials.IndexOf(':');
                    var password = decodedCredentials.Substring(separator + 1);

                    username = decodedCredentials.Substring(0, separator);

                    return username == password; //Validação em algum repositório
                }
            }

            return false;
        }

        private static string[] CarregarPermissoes(string username)
        {
            if (username == "ben-hur")
                return new[] { "Admin", "IT" };

            return new[] { "Normal" };
        }
    }
}